<?php

  include 'connection.php';

  session_start();
  //email di chi è loggato e prenota il passaggio
  $email = $_SESSION['email'];
  $id = $_GET['id'];

  try {
    //query di insert del passaggio prenotato
    $stmt = $pdo->prepare( 'INSERT INTO booked(email_user_req, id_pass) VALUES (?,?)');
    $stmt->execute([$email,$_GET['id']]);
    http_response_code(200);
  } catch (Exception $e) {
    http_response_code(500);
  }
?>
