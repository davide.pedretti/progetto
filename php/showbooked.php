<?php

include 'connection.php';

session_start();
$email = $_SESSION['email'];
header("Content-Type: application/json");

//query per mostrare i passaggi prenotati
$stmt = $pdo->prepare( "SELECT b.* FROM booked as a INNER JOIN passes as b on b.id = a.id_pass WHERE a.email_user_req = ?");
$stmt->execute([$email]);
$prenotati = [];
while($row = $stmt->fetch()){
  $prenotato = [
          'id' => $row['id'],
          'email' => $row['email'],
          'startaddress' => $row['startaddress'],
          'endaddress' => $row['endaddress'],
          'price' => $row['price'],
          'date' => $row['date']
      ];
      array_push($prenotati, $prenotato);
}
echo json_encode($prenotati);
http_response_code(200);
?>
