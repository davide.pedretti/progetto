<?php
//connessione al database car2sharedb
    $host = '127.0.0.1';
    $db   = 'car2sharedb';
    $user = 'root';
    $pass = '';
    $charset = 'utf8mb4';
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

	try {
		$pdo = new PDO($dsn, $user, $pass, $options);
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}

?>
