<?php

include 'connection.php';
session_start();
header("Content-Type: application/json");
$data = json_decode(stripslashes(file_get_contents("php://input")), true);
$email = $_SESSION["email"];
//query per mostrare i passaggi dal database ed accertarsi che un utente non possa prenotare passaggi da sè stesso 
$stmt = $pdo->prepare( "SELECT id,email,startaddress,endaddress,price,date FROM passes WHERE  email != ? and startaddress =? and endaddress =? and date=?");
$stmt->execute([$email,$data['startaddress'],$data['endaddress'],$data['date']]);
$passaggi = [];
while($row = $stmt->fetch()){
  $passaggio = [
          'id' => $row['id'],
          'email' => $row['email'],
          'startaddress' => $row['startaddress'],
          'endaddress' => $row['endaddress'],
          'price' => $row['price'],
          'date' => $row['date']
      ];
      array_push($passaggi, $passaggio);
}
echo json_encode($passaggi);
?>
