<?php

  include 'connection.php';

  if(!isset($_POST['nome'])||!isset($_POST['cognome'])||!isset($_POST['username'])
    ||!isset($_POST['email'])||!isset($_POST['password'])||!isset($_POST['cpassword'])) {
    echo "errore";
    die();
  }else{
    $name = $_POST['nome'];
    $surname = $_POST['cognome'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $cpassword = $_POST['cpassword'];
}

// controlli correttezza con le espressioni regolari
if(preg_match ( "/^[A-Za-z0-9]{4,10}$/", $name)===0){
    header("Location: /Sito/register.php?error=nameerror");
    exit;
}
if(preg_match ( "/^[A-Za-z0-9]{4,10}$/", $surname)===0){
    header("Location: /Sito/register.php?error=surnameerror");
    exit;
}
if(preg_match ( "/^[A-Za-z0-9]{4,10}$/", $username)===0){
    header("Location: /Sito/register.php?error=usernameerror");
    exit;
}
if(preg_match ( "/^[A-z0-9\.\+_-]+@[A-z0-9\._-]+\.[A-z]{2,6}$/", $email)===0){
    header("Location: /Sito/register.php?error=emailerror");
    exit;
}
if(preg_match (  "/^[A-z0-9\.\+_-]{1,25}$/", $password)===0){
    header("Location: /Sito/register.php?error=passworderror");
    exit;
}
if($password != $cpassword){
  header("Location: /Sito/register.php?error=secondpassworderror");
  exit;
}

$email = strtolower($email);

//controllo dell'unicità di email e utente
$stmt = $pdo->prepare( 'SELECT username, email FROM `user` WHERE username = ? OR email = ?');
$stmt->execute([$username,$email]);
if($stmt->fetch()){
    header("Location: /Sito/register.php?error=alredyexisting");
    die();
}else {
  $hash = password_hash($password,PASSWORD_DEFAULT);
  $query = 'INSERT INTO user (name,surname,username,email,password) VALUES (?,?,?,?,?)';
  $stmt = $pdo->prepare($query);
  $stmt->execute([$name,$surname,$username,$email,$hash]);
  header("Location: /Sito/index.php?message=registrationsuccess");
}
?>
