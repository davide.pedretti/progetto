<?php

  include 'connection.php';

  if(!isset($_POST['email'])||!isset($_POST['password'])) {
    echo "errore";
    die();
  } else{
    $email = $_POST['email'];
    $password = $_POST['password'];
}

// controlli correttezza tramite le espressioni regolari
if(preg_match ( "/^[A-z0-9\.\+_-]+@[A-z0-9\._-]+\.[A-z]{2,6}$/", $email)===0){
    $message = "La mail non è nel formato corretto";
    echo "<script type='text/javascript'>alert('$message');
          document.location='/Sito/index.php';
          </script>";
    exit;
}

if(preg_match ( "/^[A-z0-9\.\+_-]{1,25}$/", $password)===0){
    $message = "La password non è nel formato corretto";
    echo "<script type='text/javascript'>alert('$message');
          document.location='/Sito/index.php';
          </script>";
    exit;
}

//controllo dell'unicità di email e utente
$stmt = $pdo->prepare( 'SELECT * FROM `user` WHERE email=:email');
$stmt->bindParam('email',$email);
$stmt->execute();
$row = $stmt->fetch();
if(password_verify($password, $row["password"])) {
  session_start();
  $_SESSION['email']=$email;
  header("Location: /Sito/index.php");
}else {
  header("Location: /Sito/index.php?message=wrongpassword");
}


?>
