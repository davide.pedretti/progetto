<!doctype html>
<html lang="it">

<head>
	<meta charset="utf-8">
	<link rel="icon" href="favicon.ico">
	<title>Car2Share</title>
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/foglio.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="leaflet/leaflet.css" rel="stylesheet">
	<link rel="stylesheet" href="leaflet//esri-leaflet-geocoder.css">
	<link href="https://fonts.googleapis.com/css?family=Hind+Guntur|Mali" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Mali|Nanum+Gothic" rel="stylesheet">
	<!-- import della libreria jquery -->
	<script src="js/jquery-3.3.1.js"></script>

</head>

<?php
session_start();
?>

<body id="sfondo">
	<nav class="navbar navbar-expand-lg">
		<ul class="navbar-nav">
			<li id="dropd" class="nav-item">
				<!-- menu a tendina per il login -->
				<a class="nav-link dropdown-toggle whiteT" id="dropdownAccedi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Accedi
				</a>
				<div class="dropdown-menu" id="dropdownmenu" aria-labelledby="dropdownAccedi">
					<form class="px-4 py-3" novalidate method="post" action="php/logininterface.php">
						<h5 class="mb-3 text-left"> Login </h5>
						<div class="form-group">
							<label for="email">Email <span class="text-muted"></span></label>
							<div class="row">
								<div class="col-lg-2">
									<i class="fa fa-user-o fa-lg" aria-hidden="true"></i>
								</div>
								<div class="col-lg-10">
									<input name="email" type="email" id="email" placeholder="you@example.com">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="password">Password <span class="text-muted"></span></label>
							<div class="row">
								<div class="col-lg-2">
									<i class="fa fa-lock fa-lg" aria-hidden="true"></i>
								</div>
								<div class="col-lg-10">
									<input name="password" type="password" id="password" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="container" style="background-color:#f1f1f1">
    					<span class="notreg">Non sei registrato? <a href="register.php">Registrati subito!</a></span>
  					</div>
						<button type="submit" class="btn btn-primary" id="go">Go</button>
						<div class="row cellpadding" id="alertpass" style="display:none" >
							<div class="col-md-12  spazio alert alert-danger">
								<a class="blackT">Password sbagliata</a>
							</div>
						</div>
					</form>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link whiteT" id="registrati" href="register.php">Registrati</a>
			</li>
			<li class="nav-item">
				<a class="nav-link whiteT" id="logout" style="display:none" href="index.php?message=logout">Logout</a>
			</li>
		</ul>
		<!-- visualizzazione dell'email dell'utente loggato -->
		<div id="logged" style="display:none">
			<a id="user" class="whiteT">
				<?php
				  $mail = $_SESSION['email'];
					echo $mail;
				 ?>
			</a>
		</div>
	</nav>

	<div class="container">
		<div class="py-5 text-center myfont">
			<img class="d-block mx-auto mb-4" src="car.png" width="200" height="200">
			<h1 class="whiteT">CAR2SHARE</h1>
			<h2 class="whiteT"  id="benvenuto">Benvenuto!</h2>
			<h5 class="whiteT"> Parti da dove vuoi. Arrivi ovunque.</h5>
		</div>
	</div>

	<div class="row cellpadding" id="insertsuccess" style="display:none" >
		<div class="col-md-6 offset-md-3 alert alert-success">
			<a class="blackT">Passaggio inserito con successo!</a>
		</div>
	</div>

	<div class="row cellpadding" id="successo" style="display:none">
		<div class="col-md-6 offset-md-3 alert alert-success">
			<a class="blackT">Registrazione avvenuta con successo!</a>
		</div>
	</div>

	<div class="row cellpadding" id="passbooked" style="display:none" >
		<div class="col-md-6 offset-md-3 alert alert-info">
		  Complimenti! Passaggio prenotato con successo.<a href="index.php" class="alert-link"> Clicca qui</a>  per cercarne un altro!
		</div>
	</div>

	<div class="container" id="contenitore" style="display:none">
		<ul class="nav nav-tabs">
		  <li class="nav-item">
		    <a class="nav-link active blackT" id="search-tab" data-toggle="tab" href="#search" role="tab">Cerca un passaggio</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link blackT" id="give-tab" data-toggle="tab" href="#give" role="tab">Dai un passaggio</a>
		  </li>
			<li class="nav-item">
		    <a class="nav-link blackT" id="booked-tab" data-toggle="tab" href="#booked" role="tab" onclick="terzarichiesta()">Passaggi prenotati</a>
		  </li>
	  </ul>
		<div class="tab-content">
			<div class="tab-pane show active" id="search">
				<div class="row">
					<div class="col-md-4">
						<label class="whiteT" for="startAddress">Partenza da..</label>
						<input type="text" class="form-control" id="startAddress" placeholder="" value="" required>
					</div>
					<div class="col-md-4">
						<label class="whiteT" for="endAddress">Arrivo a..</label>
						<input type="text" class="form-control" id="endAddress" placeholder="" value="" required>
					</div>
					<div class="col-md-4">
						<label class="whiteT" for="searchData">Data</label>
						<!-- il minimo dell'input type "date" è stato impostato con del codice php alla data odierna -->
    				<input type="date"  id="date" min="<?php echo date('Y-m-d'); ?>" class="form-control">
				 </div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class ="row">
							<div class="col-md-6">
								<button type="button" class="btn btn-light mt-3" id="goMap">Visualizza percorso</button>
							</div>
							<div class="col-md-6">
								<button type="button" class="btn btn-light mt-3" id="goUser" data-toggle="modal" data-target="#exampleModal" onclick="primarichiesta();">
							  Visualizza passaggi
								</button>
							</div>
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content" style="width:620px;">
							      <div class="modal-header">
							        <h5 class="modal-title">Passaggi disponibili</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
											<div class="table-responsive-md">
												<!-- tabella che verrà riempita in request.js e conterrà i passaggi disponibili -->
												<table class="table" id="myTable" style="width:600px;">
													<thead>
														<tr>
															<th scope="col">email</th>
															<th scope="col">Partenza</th>
															<th scope="col">Arrivo</th>
															<th scope="col">Prezzo</th>
															<th scope="col">Data</th>
															<th scope="col">&nbsp</th>
														</tr>
													</thead>
													<tbody id="tablebody">
													</tbody>
												</table>
											</div>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
							      </div>
							    </div>
							  </div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="padding-top:20px;">
					<div class="col-md-12 mt-1" id="mapid" style="height: 440px; border: 1px solid #AAA;">
					</div>
				</div>
			</div>
			<div class="tab-pane" id="give">
				<form class="px-4 py-3" method="post" action="php/insertpasses.php">
					<div class="row">
						<div class="col-md-3">
							<label class="whiteT" for="startAddressGive">Partenza da..</label>
							<input type="text" class="form-control" name="startAddressGive" placeholder="" value="" required>
						</div>
						<div class="col-md-3">
							<label class="whiteT" for="endAddressGive">Arrivo a..</label>
							<input type="text" class="form-control" name="endAddressGive" placeholder="" value="" required>
						</div>
					 <div class="col-md-3">
						 <label class="whiteT" for="price">Prezzo (€)</label>
						 <input type="number" style="text-align:right;" class="form-control" id="price" name="price" placeholder="" value="" required>
					 </div>
					 <div class="col-md-3">
						 <label class="whiteT" for="searchData">Data</label>
						 <!-- il minimo dell'input type "date" è stato impostato con del codice php alla data odierna -->
						 <input type="date" class="form-control" min="<?php echo date('Y-m-d'); ?>" name="date">
					</div>
					 <div class="col-md-12" >
						 <button type="submit" class="btn btn-light mt-3">Inserisci passaggio</button>
					 </div>
				</form>
				</div>
			</div>
			<div class="tab-pane" id="booked">
				<!-- tabella che verrà riempita in request.js e conterrà i passaggi prenotati -->
				<table class="table table-bordered table-dark" id="tblbooked">
					<thead class="thead-dark">
						<tr>
							<th scope="col">email offerente</th>
							<th scope="col">Partenza</th>
							<th scope="col">Arrivo</th>
							<th scope="col">Prezzo</th>
							<th scope="col">Data</th>
						</tr>
					</thead>
					<tbody id="bookedtablebody">
					</tbody>
				</table>
			</div>
		<hr>
	</div>

	<footer class="page-footer col-md-12 text-muted text-center text-small">
		<p class="mb-1 whiteT">&copy; 2018-19 Car2Share</p>
	</footer>

	<?php
	if(isset($_GET["message"]))
	{
		if($_GET["message"] == 'wrongpassword'){
			echo '<script>
						$("#dropdownmenu").show();
		        $("#alertpass").show();
		        </script>';
		}elseif($_GET["message"] == 'insertsuccess'){
			echo '<script>
						$("#benvenuto").hide();
						$("#insertsuccess").show();
						$("#passbooked").hide();
		        </script>';
		}elseif($_GET["message"] == 'registrationsuccess'){
			echo '<script>
						$("#benvenuto").hide();
						$("#successo").show();
						</script>';
		}
		else{
			unset($_SESSION["email"]);
		}
	}

	if (isset($_SESSION['email']))
	{
		echo '<script>
		 		$("#logged").show();
				$("#dropd").hide();
				$("#logout").show();
				$("#contenitore").show();
				$("#registrati").hide();
				 </script>';
	}
	?>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/global.js"></script>
	<script src="js/request.js"></script>
	<script src="leaflet/leaflet.js"></script>
	<script src="leaflet/leaflet-providers-master/leaflet-providers.js"></script>
	<script src="leaflet/esri-leaflet.js"></script>
	<!-- Esri Leaflet Geocoder -->
	<script src="leaflet/esri-leaflet-geocoder.js"></script>
</body>
</html>
