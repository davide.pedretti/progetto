var richiesta;
var indirizzo;
var table;
var tbody;

function primarichiesta() {
  //recupero gli elementi aventi come id startAddress, endAddress e date
  var cittapartenza = document.getElementById('startAddress').value;
  var cittaarrivo =  document.getElementById('endAddress').value;
  var data =  document.getElementById('date').value;
  var pack = {};
  pack['startaddress'] = cittapartenza;
  pack['endaddress'] = cittaarrivo;
  pack['date'] = data;
  pack = JSON.stringify(pack);
  richiesta= new XMLHttpRequest(); //creo richiesta
  indirizzo= "/Sito/php/showpasses.php";
  richiesta.open('POST', indirizzo, true); //inizializzo la richiesta
  richiesta.setRequestHeader("Content-Type", "application/json"); //stabilisce il valore di un header HTTP di richiesta
  richiesta.responseType = 'json'; //specifico il tipo di dato contenuto nella risposta
  richiesta.onload= function (){
    table = document.getElementById("myTable");
    tbody = document.getElementById("tablebody");
    numPassaggi = richiesta.response.length;
    for (let i=0; i<numPassaggi; i++){
      var row = document.createElement("tr");
      var td = document.createElement("td");
      td.innerHTML = richiesta.response[i].email;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = richiesta.response[i].startaddress;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = richiesta.response[i].endaddress;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = richiesta.response[i].price;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = richiesta.response[i].date;
      row.appendChild(td);
      td = document.createElement("td");
      var selectedId = richiesta.response[i].id;
      let bottone= document.createElement("button");
      bottone.innerHTML= "Prenota";
      bottone.setAttribute("class", "miobottone btn btn-primary");
      bottone.addEventListener("click", function(){secondarichiesta(selectedId);});
      row.appendChild(bottone);
      row.appendChild(td);
      tbody.appendChild(row);
    }
  }
  richiesta.send(pack);
  svuotaTabella("tablebody");
}

function secondarichiesta(selectedId) {
  richiesta = new XMLHttpRequest(); //creo richiesta
  indirizzo = "/Sito/php/insertbookedpasses.php?id="+selectedId;
  richiesta.open('GET', indirizzo, true); //inizializzo la richiesta
  richiesta.onload = function() {
    //se si è verificato un errore 500 lato server, visualizzo l'alert di errore ed esco dalla funzione
    if(richiesta.status == 500) {
      alert("error");
      return;
    }
    //quando la prenotazione va a buon fine, nascondo il modal e mostro il messaggio di successo
    $("#exampleModal").modal('hide');
    $("#passbooked").show();
    $("#insertsuccess").hide();

  }
  richiesta.send();
}

function terzarichiesta() {
  richiesta= new XMLHttpRequest(); //creo richiesta
  indirizzo= "/Sito/php/showbooked.php";
  richiesta.open('POST', indirizzo, true); //inizializzo la richiesta
  richiesta.setRequestHeader("Content-Type", "application/json");
  richiesta.responseType = ''; //specifico il tipo di dato contenuto nella risposta
  richiesta.onload= function (){
    table = document.getElementById("tblbooked");
    tbody = document.getElementById("bookedtablebody");
    var data = JSON.parse(richiesta.responseText.trim());
    var numPassaggi = data.length;
    for (let i=0; i<numPassaggi; i++){
      var row = document.createElement("tr");
      var td = document.createElement("td");
      td.innerHTML = data[i].email;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = data[i].startaddress;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = data[i].endaddress;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = data[i].price;
      row.appendChild(td);
      td = document.createElement("td");
      td.innerHTML = data[i].date;
      row.appendChild(td);
      tbody.appendChild(row);
    }
  }
  richiesta.send();
  svuotaTabella("bookedtablebody");
}

//funzione per pulire la tabella dalle righe esistenti
function svuotaTabella(body) {
  document.getElementById(body).innerHTML="";
}
