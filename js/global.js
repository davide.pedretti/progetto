$(document).ready(function() {

  var latitudine = 45.4632;
  var longidutine = 9.1780;

  var map = L.map('mapid', {
    center: [45.4632, 9.1780],
    minZoom: 2,
    zoom: 13
  });

  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { //funziona anche senza {s}
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a', 'b', 'c']
  }).addTo(map);

  var geocodeService = L.esri.Geocoding.geocode();

  setInterval(function() {
    map.invalidateSize();
  }, 100);
  //variabili per i marcatori delle due città e per il collegamento tra di esse
  var markerStartCity;
  var markerEndCity;
  var polyline;
 //quando si clicca il bottone avente come id "goMap" viene costruito il collegamento tra le città 
  $("#goMap").click(function() {

    var startPolyline;
    var endPolyline;

    geocodeService.city($("#startAddress").val()).run(function(err, results, response) {
      console.log(results);
      if (results.results.length > 0) {
        var r = results.results[0];
        var lat = r.latlng.lat;
        var lon = r.latlng.lng;
        startPolyline = r.latlng;
        console.log('Latitudine Start Address:' + lat);
        console.log('Longitudine Start Address:' + lon);

        var redIcon = new L.Icon({
          iconUrl: 'leaflet/images/marker-icon-2x-red.png',
          shadowUrl: 'leaflet/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41]
        });

        if (markerStartCity != null && markerStartCity != undefined) {
          map.removeLayer(markerStartCity);
        }

        markerStartCity = L.marker([lat, lon], {
          icon: redIcon
        }).addTo(map);
      }
    });

    geocodeService.city($("#endAddress").val()).run(function(err, results, response) {
      console.log(results);
      if (results.results.length > 0) {
        var r = results.results[0];
        var lat = r.latlng.lat;
        var lon = r.latlng.lng;
        endPolyline = r.latlng;
        console.log('Latitudine End Address:' + lat);
        console.log('Longitudine End Address:' + lon);

        var greeIcon = new L.Icon({
          iconUrl: 'leaflet/images/marker-icon-2x-green.png',
          shadowUrl: 'leaflet/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41]
        });
        if (markerEndCity != undefined || markerEndCity !=null ) {
          map.removeLayer(markerEndCity);
        }
        markerEndCity = L.marker([lat, lon], {
          icon: greeIcon
        }).addTo(map);
      }
    });

    setTimeout(function() {
      var polylinePoints = [startPolyline, endPolyline];

      var polylineOptions = {
        color: 'blue',
        weight: 6,
        opacity: 0.9
      };

      if (polyline != null && polyline != undefined) {
        map.removeLayer(polyline);
      }
      polyline = new L.Polyline(polylinePoints, polylineOptions);

      var tempLatLng = null;
      var totalDistance = 0.00000;

      tempLatLng = startPolyline;
      totalDistance += tempLatLng.distanceTo(endPolyline);
      //conversione della distanza in km
      var newTotalDistance = ((totalDistance).toFixed(1))/1000

      polyline.bindPopup(newTotalDistance+ ' km');

      map.addLayer(polyline);

      // zoomma la mappa
      map.fitBounds(polyline.getBounds());

      polyline.openPopup();

    }, 1000);
  });

})
