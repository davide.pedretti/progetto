<!doctype html>
<html lang="it">

	<head>
		<meta charset="utf-8">
		<link rel="icon" href="favicon.ico">
        <title>Car2Share</title>
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/foglio.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Hind+Guntur|Mali" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Mali|Nanum+Gothic" rel="stylesheet">
	</head>

<body id="sfondo">
	<nav class="navbar navbar-expand-lg">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link whiteT" href="index.php">Home</a>
			</li>
			<li id="dropd" class="nav-item">
				<a class="nav-link dropdown-toggle whiteT" id="dropdownAccedi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Accedi
				</a>
				<div class="dropdown-menu" id="dropdownmenu" aria-labelledby="dropdownAccedi">
					<form class="px-4 py-3" method="post" action="php/logininterface.php">
						<h5 class="mb-3 text-left"> Login </h5>
						<div class="form-group">
							<label for="email">Email <span class="text-muted"></span></label>
							<div class="row">
								<div class="col-lg-2">
									<i class="fa fa-user-o fa-lg" aria-hidden="true"></i>
								</div>
								<div class="col-lg-10">
									<input name="email" type="email" id="emailregistration" placeholder="you@example.com">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="password">Password <span class="text-muted"></span></label>
							<div class="row">
								<div class="col-lg-2">
									<i class="fa fa-lock fa-lg" aria-hidden="true"></i>
								</div>
								<div class="col-lg-10">
									<input name="password" type="password" id="password" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="container" style="background-color:#f1f1f1">
    					<span class="notreg">Non sei registrato? <a href="register.php">Registrati subito!</a></span>
  					</div>
						<button type="submit" class="btn btn-primary" id="go">Go</button>
						<div class="row cellpadding" id="alertpass" style="display:none" >
							<div class="col-md-12  spazio alert alert-danger">
								<a class="blackT">Password sbagliata</a>
							</div>
						</div>
					</form>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link whiteT" href="register.php">Registrati</a>
			</li>
		</ul>
	</nav>

    <div class="container" id="registration">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<div class="py-5 text-center myfont">
						<img class="d-block mx-auto mb-4" src="car.png" width="100" height="100">
						<h1 class="whiteT">Registrati </h1>
						<div class="col-lg-12">
							<i class="fa fa-address-card-o fa-2x" aria-hidden="true"></i>
							<h2 class="lead whiteT">Completa i campi sottostanti per registrarti</h2>
						</div>
					</div>
				</div>
			</div>

			<!-- div per messaggi di errore -->
			<div class="row cellpadding" id="alertregistrazione" >
				<div class="col-md-6 offset-md-3  spazio alert alert-danger">
					<p id="errore"> </p>
				</div>
			</div>

		<div  class="row cellpadding">
			<div class="col-md-6 offset-md-3">
					<form method="post" action='php/registration.php'>
						<label for="nome">Nome</label>
						<input name="nome" type="text" class="form-control" placeholder="Nome" required>
						<label for="cognome">Cognome</label>
						<input name="cognome"type="text" class="form-control" placeholder="Cognome" required>
						<label for="username">Username</label>
						<input name="username"type="text" class="form-control" placeholder="Username" required>
						<label for="email">Email <span class="text-muted"></span></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">@</span>
							</div>
							<input name="email"type="text" class="form-control" placeholder="you@example.com">
						</div>
						<label for="password">Password <span class="text-muted"></span></label>
						<input name="password" type="password" class="form-control" placeholder="Password">
						<label for="cpassword">Conferma Password <span class="text-muted"></span></label>
						<input name="cpassword" type="password" class="form-control" placeholder="ConfermaPassword">
	          <br>
						<button id="gor" type="submit" class="btn btn-light">Go</button>
					</form>
			</div>
		</div>
	</div>
      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1 whiteT">&copy; 2018-19 Car2Share</p>
      </footer>

		<?php
		// controllo della presenza nell'URL di "message" o "error" e display di ciò che deve essere mostrato a seconda dei casi 
		if(isset($_GET["message"]))
		{
			if($_GET["message"] == 'wrongpassword'){
				echo '<script>
							$("#dropdownmenu").show();
			        $("#alertpass").show();
			        </script>';
			}else{
				unset($_SESSION["email"]);
			}
		}

		if (isset($_SESSION['email']))
		{
			echo '<script>
			 		$("#logged").show();
					$("#dropd").hide();
					$("#logout").show();
					$("#contenitore").show();
					 </script>';
		}

		?>
	<script src="js/jquery-3.3.1.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
	var error = <?php
	    if(isset($_GET['error'])){
	      echo '"'.$_GET['error'].'"';
	    }else{
	      echo 0;
	    } ?>;
	var alertregistrazione = document.getElementById('alertregistrazione');
	alertregistrazione.style.display = "initial";
	var errore = document.getElementById('errore');
	switch(error){
	    case "nameerror":
	      errore.innerHTML = "Il nome non è nel formato valido. Riprovare";
	      break;
	    case "surnameerror":
	      errore.innerHTML = "Il cognome non è nel formato valido. Riprovare.";
	      break;
	    case "usernameerror":
	      errore.innerHTML = "Lo username non è nel formato valido. Riprovare.";
	      break;
	    case "emailerror":
	      errore.innerHTML = "La mail non è nel formato valido. Riprovare.";
	      break;
	    case "passworderror":
	      errore.innerHTML = "La password non è nel formato valido. Riprovare.";
	      break;
	    case "secondpassworderror":
	      errore.innerHTML = "Password non coincidenti";
	      break;
	    case "alredyexisting":
	      errore.innerHTML = "Utente già esistente";
	      break;
	    default:
	      alertregistrazione.style.display = "none";
	      break;
	  }
  </script>
  </body>
</html>
