-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 04, 2019 alle 18:22
-- Versione del server: 10.1.38-MariaDB
-- Versione PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car2sharedb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `booked`
--

CREATE TABLE `booked` (
  `id` int(11) NOT NULL,
  `email_user_req` varchar(50) NOT NULL,
  `id_pass` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `booked`
--

INSERT INTO `booked` (`id`, `email_user_req`, `id_pass`) VALUES
(48, 'michele@libero.com', 15),
(49, 'matteo@libero.com', 18),
(52, 'matteo@libero.com', 24),
(54, 'matteo@libero.com', 20),
(55, 'matteo@libero.com', 18),
(56, 'matteo@libero.com', 19),
(57, 'michele@libero.com', 27),
(59, 'benedetta@gmail.com', 18),
(60, 'benedetta@gmail.com', 24),
(61, 'davidebro@gmail.com', 17),
(62, 'michele@libero.com', 28),
(63, 'davide9@icloud.com', 29),
(64, 'davide9@icloud.com', 17),
(65, 'davide9@icloud.com', 28),
(66, 'davide9@icloud.com', 20),
(67, 'davide9@icloud.com', 23);

-- --------------------------------------------------------

--
-- Struttura della tabella `passes`
--

CREATE TABLE `passes` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `startaddress` varchar(20) NOT NULL,
  `endaddress` varchar(20) NOT NULL,
  `price` int(4) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `passes`
--

INSERT INTO `passes` (`id`, `email`, `startaddress`, `endaddress`, `price`, `date`) VALUES
(15, 'matteo@libero.com', 'Milano', 'Brescia', 10, '2019-06-05'),
(16, 'matteo@libero.com', 'Brescia', 'Verona', 12, '2019-06-09'),
(17, 'matteo@libero.com', 'Brescia', 'Pavia', 12, '2019-06-12'),
(18, 'michele@libero.com', 'Milano', 'Brescia', 9, '2019-06-04'),
(19, 'michele@libero.com', 'Milano', 'Mantova', 15, '2019-06-20'),
(20, 'michele@libero.com', 'Milano', 'Bergamo', 12, '2019-06-20'),
(21, 'matteo@libero.com', 'Milano', 'Padova', 19, '2019-06-14'),
(22, 'matteo@libero.com', 'Milano', 'Venezia', 29, '2019-06-20'),
(23, 'matteo@libero.com', 'Bergamo', 'Brescia', 15, '2019-06-20'),
(24, 'michele@libero.com', 'Brescia', 'Verona', 7, '2019-06-09'),
(25, 'matteo@libero.com', 'Milano', 'Bergamo', 19, '2019-06-27'),
(27, 'matteo@libero.com', 'Bergamo', 'Piacenza', 20, '2019-06-07'),
(28, 'benedetta@gmail.com', 'Brescia', 'Piacenza', 15, '2019-06-28'),
(29, 'davidebro@gmail.com', 'Milano', 'Bolzano', 35, '2019-06-28'),
(30, 'davide9@icloud.com', 'Bolzano', 'Merano', 20, '2019-07-19');

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`user_id`, `name`, `surname`, `username`, `email`, `password`) VALUES
(41, 'Matteo', 'Foresti', 'Matteo1', 'matteo@libero.com', '$2y$10$uLgJ8NIt/ss9HntzoCez4OCAj5.RvZZIgSeGrmz7hcWEd34UbIJ1y'),
(42, 'Michele', 'Cozzatti', 'Michele123', 'michele@libero.com', '$2y$10$q5d3zb56F5Nb0gnYoJUdbOIBU4r4zvsmNHuGvAafh2H3WtlO3LVOe'),
(43, 'Benedetta', 'Ghedi', 'Benedetta9', 'benedetta@gmail.com', '$2y$10$KIE0iFPbnpWvyk.CmlJ9PewbzfSnAskZ18F/9IfPOAbaW2YQwD6bS'),
(44, 'Davide', 'Brognoli', 'DavideBRO', 'davidebro@gmail.com', '$2y$10$e3C8iHuR.m458zLvek9MBeRoXSQENAEfl82rtTB5/flW.HuikC4su'),
(45, 'Davide', 'Pedretti', 'Davide12', 'davide9@icloud.com', '$2y$10$DzjYKFnS.CY7lL2HYb5pjuQ7AqY4vobjzD/.0zwd/t7HRKKHWA8Qq');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `booked`
--
ALTER TABLE `booked`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pass` (`id_pass`),
  ADD KEY `email booked` (`email_user_req`);

--
-- Indici per le tabelle `passes`
--
ALTER TABLE `passes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_2` (`email`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `booked`
--
ALTER TABLE `booked`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT per la tabella `passes`
--
ALTER TABLE `passes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `booked`
--
ALTER TABLE `booked`
  ADD CONSTRAINT `email booked` FOREIGN KEY (`email_user_req`) REFERENCES `user` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id passaggio` FOREIGN KEY (`id_pass`) REFERENCES `passes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `passes`
--
ALTER TABLE `passes`
  ADD CONSTRAINT `email` FOREIGN KEY (`email`) REFERENCES `user` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
